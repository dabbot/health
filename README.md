## Identifying

To identify, send a HELLO message:  
`HELLO:SERVICE_NAME:1\n`, where `:` acts as delimiter.

First part of the message is always the message type, in case of identification, it is HELLO.  
Second part of the identification message is the service name.  
Third part is the service ID.

Example for shards:  
`HELLO:SHARD:218\n`

Example for lavalink nodes:  
`HELLO:LAVALINK-NODE:43\n`

Example for queues:  
`HELLO:QUEUE:4\n`

*All of the above parts are required for proper identification!*

There is a fourth part of the message, for passing the service status code.  
`HELLO:WORKER:8:1\n`

See potential statuses below.

## Health checks

After identifying, the service will receive a `PING\n` message. The service is required to respond with `PONG\n` within 30 seconds of receiving this message.

If necessary, you may send a service status code as the second part of the message after a `:` delimiter.  
Example: `PONG:1\n` to identify a service as being online.

*If no status code is sent, it is assumed the status code is 1!*

You may also send a status message: `PONG:1:Your message here.\n` You *must* include a status code with this message!

## Status codes

* 0 - offline (not existing and not running, sent if going offline)
* 1 - online (exists and running)
* 2 - created (exists but not running yet)
* 3 - restarting
* 4 - high load
* 5 - error (any / catch all, include message)