const net = require('net');

function main(i) {
    const client = new net.Socket();
    client.connect(5002, '127.0.0.1', () => {
        console.log('connected');
        client.write("HELLO:LAVALINK-QUEUE:" + i + ":2");
    });

    client.on('data', data => {
        data = data.toString('utf8');
        console.log(data);
        if(data.startsWith('PING')) {
            client.write("PONG:This is a status message\n");
        }
    })

    client.on('end', () => {
        console.log('connection closed');
    })
    console.log('listening');
}

for(let i = 1; i < 3; i++) {
    main(i);
}
