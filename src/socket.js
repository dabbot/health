const net = require('net');
const http = require('http');
const clients = new Map();
const timeouts = new Map();

function main() {
    net.createServer(socket => socketHandler(socket)).listen(5002);
    http.createServer((req, res) => httpHandler(req, res)).listen(3002);
    console.log('listening');
}

function socketHandler(socket) {
    socket.on('data', data => handleData(socket, data));
    socket.on('end', data => handleEnd(socket, data));
}

function handleData(socket, data) {
    data = data.toString('utf8');
    data = data.replace(/(\r\n\t|\n|\r\t)/gm,"");
    const timer = timeouts.get(socket.identifier);
    if(data.startsWith('PONG') && clients.get(socket.identifier)) {
        if(!timer) return;
        if(!socket.times) socket.times = [];
        if(!socket.statusMessages) socket.statusMessages = [];
        if(socket.times.length > 99) socket.times.pop();
        const diff = process.hrtime(timer.date);
        socket.times.push(diff[0] * 1e9 + diff[1]);
        clearInterval(timer.timeout);
        data = data.split(':'); 
        if(!data[1]) return socket.statusMessage = 1;
        socket.statusMessage = data[1];
        if(data[2]) socket.statusMessages.push(data[2]);
        if(socket.statusMessages > 49) socket.statusMessages.pop();
    }
    else if(data.startsWith('HELLO')) {
        try {
            data = data.split(':');
            socket.ok = true;
            if(data[3]) socket.statusCode = data[3];
            socket.identifier = data[1] + data[2];
            clients.set(socket.identifier, socket);
            if(timer) clearInterval(timer.timeout);
        } catch(e) {
            console.error(e);
        }
    }
}

function handleEnd(socket) {
    if(socket.identifier) {
        socket.statusCode = 0;
        clients.set(socket.identifier, socket);
        if(timeouts.get(socket.identifier)) clearInterval(timeouts.get(socket.identifier).timeout);
    }
}

function httpHandler(req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.writeHead(200, { 'Content-Type': 'application/json'});
    let data = {};
    for(let [k, v] of clients) {
        data[k] = v;
    }
    res.end(JSON.stringify(Object.values(data)));
}

main();

setInterval(() => {
    clients.forEach(socket => {
        try {
            if(!socket.ok && socket.identifier) return;
            socket.write("PING\n");

            timeouts.set(socket.identifier, {
                timeout: setTimeout(() => {
                    handleEnd(socket);
                }, 30000),
                date: process.hrtime()
            })
        } catch(e) {
            console.error(e);
        }
    });
}, 5000);
