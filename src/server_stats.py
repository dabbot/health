from sanic import Sanic
from sanic_cors import CORS, cross_origin
from sanic.response import json
import psutil

app = Sanic()
CORS(app, resources={r"/*": {"origins": "*"}})

def get_mem_percent():
    return psutil.virtual_memory().percent

@app.route('/stats')
async def stats(request):
    return json({
        'cpu': psutil.cpu_percent(),
        'memory': get_mem_percent(),
        'net_sent': psutil.net_io_counters().bytes_sent

    })

app.run(host='localhost', port=3003)

